//
//  main.m
//  NikiTestObjectiveC
//
//  Created by Abhinav Singh on 9/8/17.
//  Copyright © 2017 Niki.ai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
