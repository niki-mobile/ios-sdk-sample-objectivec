//
//  ViewController.m
//  NikiTestObjectiveC
//
//  Created by Abhinav Singh on 9/8/17.
//  Copyright © 2017 Niki.ai. All rights reserved.
//

#import "ViewController.h"
#import "ChatNiki-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)chatWithNiki:(id)sender {
    
    ChatNiki *chat = [[ChatNiki alloc] init];
    [chat initiateNikiWithViewController:self];
}


@end
