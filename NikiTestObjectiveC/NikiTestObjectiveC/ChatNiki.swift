//
//  ChatNiki.swift
//  NikiTestObjectiveC
//
//  Created by Abhinav Singh on 9/8/17.
//  Copyright © 2017 Niki.ai. All rights reserved.
//

import UIKit
import nikiSDK

@objc class ChatNiki: NSObject {
    
    @objc func initiateNiki(viewController:UIViewController){
        
        let sessionConfig = SessionConfig(accessKey:"1480670100",secret:"f2IRd4YC552QZAqu2DThFdzwYAHyuxbu1NfVJghYazw=",merchantTitle:"NikiTest")
        NikiConfig.sharedInstance.initNiki(sessionConfig: sessionConfig)
        NikiConfig.sharedInstance.startChat(sessionConfig: sessionConfig,viewController: viewController)
    }
    
    @objc func onHandlePayment(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool{
        return NikiConfig.sharedInstance.onHandlePaymentRedirectURL(app,open:url,options:options)
    }
    
}
