//
//  AppDelegate.h
//  NikiTestObjectiveC
//
//  Created by Abhinav Singh on 9/8/17.
//  Copyright © 2017 Niki.ai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

